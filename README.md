# CloudAssignment

**MVVM Design Pattern**

- Using Closure for data binding.
- DetailViewModel: Take charge of getting photos from API manager and using closure for data binding..
- Data Binding: Using input, output, transform pattern and closure for data binding.

---

**Api Layer**

- APIManager: Using Alamofire and OHHttpstubs for mock data and Unit Testing.
- APIConfig: The base endpoint and other api paths.
- APIResponseModel: the API response model.


---

**Mock Data**

- Using OHHttpstubs for Mock data.
- MockDataManager: Wrap OHHttpstubs.

---

**ImageCacheManager**

- SAImageCacheManager: Using Etag and If-None-Match to reload if the image resource is update.
- SACacheManager: Using NSCache for cache.

---

**Unit Testing**

- Project Code Coverage average: 26.8%.
- ViewModel: Coverage average: 100.0% (DetailViewModel).
