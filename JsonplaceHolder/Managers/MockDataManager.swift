//
//  MockDataManager.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/31.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import Foundation
import OHHTTPStubs

public final class MockDataManager {
    
    static let shared = MockDataManager()
    
    func setUpAllMockData() {
        setupPhotoResponse()
    }
    
    func removeAllMockData() {
        HTTPStubs.removeAllStubs()
    }
    
    func setupPhotoResponse() {
        guard let photoResponse = self.queryPhotoResponse() else { return }
        HTTPStubs.stubRequests(passingTest: { (urlRequest) -> Bool in
            let path = urlRequest.url?.absoluteString ?? ""
            return path.contains("photos")
            
        }) { (urlRequest) -> HTTPStubsResponse in
            return HTTPStubsResponse.init(jsonObject: photoResponse, statusCode: 200, headers: nil)
        }
    }
    
    func queryPhotoResponse() -> Any? {
        let photosFilePath = Bundle.main.path(forResource: "PhotosResponse_success.json", ofType: nil) ?? ""
        guard let photosData = NSData(contentsOfFile: photosFilePath) else { return nil }
        do {
            let photosResponse = try JSONSerialization.jsonObject(with: photosData as Data, options: .allowFragments)
            return photosResponse
        } catch {
            return nil
            
        }
    }
}
