//
//  SAImageCacheManager.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/30.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import Foundation
import UIKit

/**
簡略描述此方法: light weight tool to cache image and present it, this tool using E-tag mechanism to implement cache.
- Parameters:
  - withUrl: 參數1, your image url.
  - cacheKey: 參數2, This is an optional parameter, while your url may change all the time cause by apiKey changed, quering string changed or some other reason, you can set this value for cache image key.
  - parameters: 參數3, GET request parameters.
  - isReturnCachedImageAsDefault: 參數4, Set true will return cached image immediately. (true by default)
- Returns: 回傳值
*/
final public class SACacheManager {
    static let shared = SACacheManager()
    let saCache: NSCache = { () -> NSCache<AnyObject, AnyObject> in
        let cache = NSCache<AnyObject, AnyObject>()
        cache.name = "MykizpadCache"
        cache.countLimit = 5000 // Max 5000 images in memory.
        cache.totalCostLimit = 128*1024*1024 // Max 128MB used.
        
        return cache
    }()
}
final public class SAImageCacheManager {
//    private init() {}
    let keyOfEtagInHeader = "Etag"
    let keyOfCacheKey = "CacheKey"
    let keyOfIfNoneMatch = "If-None-Match"
    let noCachedEtag = "no Etag cached"
    let noCachedCacheKey = "no CacheKey cached"
    var task: URLSessionDataTask?
    let sharedkizpadCache: NSCache = SACacheManager.shared.saCache

    /* fetchCachedDataByEtag
     set cacheKey if your url parameter will change all the time
     set shouldValidRemoteEtag to false if you don't want to check the remote Etag, if the image cached in local is exist it will be return otherwise the request will be launch

     4/26 update
     now will response cache Image immediately if the url or cacheKey exist in local, Than the request will be launch with eTag parameter, If header response 200, we will cache new image and this new image will be serve, if response 304 we just print a log cause the image is the same with local cache which already be serve at first time.
     */




    /// light weight tool to cache image and present it, this tool using E-tag mechanism to implement cache.
    ///
    /// - parameter withUrl: your image url.
    /// - parameter cacheKey: This is an optional parameter, while your url may change all the time cause by apiKey changed, quering string changed or some other reason, you can set this value for cache image key. **Use your url path without parameters is recommend!**
    /// - parameter parameters: GET request parameters.
    /// - parameter isReturnCachedImageAsDefault: Set true will return cached image immediately. (true by default)
    ///
    ///
    /**
    簡略描述此方法: light weight tool to cache image and present it, this tool using E-tag mechanism to implement cache.
    - Parameters:
      - withUrl: 參數1, your image url.
      - cacheKey: 參數2, This is an optional parameter, while your url may change all the time cause by apiKey changed, quering string changed or some other reason, you can set this value for cache image key.
      - parameters: 參數3, GET request parameters.
      - isReturnCachedImageAsDefault: 參數4, Set true will return cached image immediately. (true by default)
    - Returns: 回傳值
    */
    func fetchCachedDataByEtag(withUrl url: String, cacheKey: String? , parameters: [String: String]?, isReturnCachedImageAsDefault: Bool = true, completion: @escaping (_ data: Data?, _ requestURL: String, _ response: URLResponse?, _ error: Error?) -> Void) {
        
        guard let dataUrl = URL(string: url) else {
            print("url foremat error")
            return
        }

        var cachedCacheKey = ""
        var keyForCache = ""
        if (cacheKey != nil) {
            cachedCacheKey = self.sharedkizpadCache.object(forKey: cacheKey! + keyOfCacheKey as AnyObject) as? String ?? noCachedCacheKey
            keyForCache = cacheKey!
        } else {
            cachedCacheKey = self.sharedkizpadCache.object(forKey: url + keyOfCacheKey as AnyObject) as? String ?? noCachedCacheKey
            keyForCache = url
        }


        if isReturnCachedImageAsDefault && keyForCache == cachedCacheKey {
            if cacheKey != nil {
                if let data = self.sharedkizpadCache.object(forKey: cacheKey! as AnyObject) as? Data {
                    completion(data, url, nil, nil)
                    print("return data in local cache without validRemoteEtag")
                }
            } else {
                if let data = self.sharedkizpadCache.object(forKey: url as AnyObject) as? Data {
                    completion(data, url, nil, nil)
                    print("return data in local cache without validRemoteEtag")
                }
            }
        }

        var request = URLRequest(url: dataUrl)
        var cachedEtag = ""
        if let _ = cacheKey {
            cachedEtag = self.sharedkizpadCache.object(forKey: cacheKey! + keyOfEtagInHeader as AnyObject) as? String ?? noCachedEtag
        } else {
            cachedEtag = self.sharedkizpadCache.object(forKey: url + keyOfEtagInHeader as AnyObject) as? String ?? noCachedEtag
        }

        if let para = parameters {
            for (key, value) in para {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }

        request.addValue(cachedEtag, forHTTPHeaderField: keyOfIfNoneMatch)
        print("request addValue: \(cachedEtag) forHTTPHeaderField: \(keyOfIfNoneMatch)")
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData

        task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil{
                completion(data, url, response, error)
                print(error as Any)
            } else {
                if let response = response as? HTTPURLResponse {
                    let statusCode = response.statusCode
                    print("status code = \(statusCode)")
                    switch statusCode {
                    case 200:
                        if let data = data {
                            if let _ = cacheKey {
                                self.sharedkizpadCache.setObject(data as AnyObject, forKey: cacheKey! as AnyObject, cost: data.count)
                            } else {
                                self.sharedkizpadCache.setObject(data as AnyObject, forKey: url as AnyObject, cost: data.count)
                            }
                            if let eTagInReturnHeader = response.allHeaderFields[self.keyOfEtagInHeader] {
                                if let _ = cacheKey {
                                    self.sharedkizpadCache.setObject(eTagInReturnHeader as AnyObject, forKey: cacheKey! + self.keyOfEtagInHeader as AnyObject, cost: data.count)
                                    self.sharedkizpadCache.setObject(cacheKey! as AnyObject, forKey: cacheKey! + self.keyOfCacheKey as AnyObject, cost: data.count)
                                } else {
                                    self.sharedkizpadCache.setObject(eTagInReturnHeader as AnyObject, forKey: url + self.keyOfEtagInHeader as AnyObject, cost: data.count)
                                    self.sharedkizpadCache.setObject(url as AnyObject, forKey: url + self.keyOfCacheKey as AnyObject, cost: data.count)
                                }

                            }
                            DispatchQueue.main.async {
                                completion(data, url, response, error)
                                print("return data from server")
                            }
                        }
                    case 304:
                        if !isReturnCachedImageAsDefault{
                            if let _ = cacheKey {
                                if let data = self.sharedkizpadCache.object(forKey: cacheKey! as AnyObject) as? Data {
                                    completion(data, url, response, error)
                                    print("return data in local cache")
                                }
                            } else {
                                if let data = self.sharedkizpadCache.object(forKey: url as AnyObject) as? Data {
                                    completion(data, url, response, error)
                                    print("return data in local cache")
                                }
                            }
                        }
                        print("return data in local cache")
                    default:
                        print("please handle this status code: \(statusCode)")
                        completion(data, url, response, error)
                    }
                }
            }
        }
        task?.resume()
    }
    
    func cancelSession() {
        task?.cancel()
    }
    
    func suspendSession() {
        task?.suspend()
    }
}
