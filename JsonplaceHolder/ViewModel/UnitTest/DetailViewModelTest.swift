//
//  DetailViewModelTest.swift
//  JsonplaceHolderTests
//
//  Created by CI-Red.Tseng on 2020/8/31.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import XCTest
@testable import JsonplaceHolder

class DetailViewModelTest: XCTestCase {

    var detailViewModel: DetailViewModel!
    
    override func setUp() {
        super.setUp()
        let apiManager = APIManager(isMock: true)
        detailViewModel = DetailViewModel(with: apiManager)
    }
    
    override func tearDown() {
        detailViewModel = nil
        super.tearDown()
    }
    
//    func testExample() throws {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//
//    func testPerformanceExample() throws {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
    func testGetThumbnailID(){
        let expectation = self.expectation(description: "Async call")
        self.detailViewModel.input.viewWillAppear!()
        detailViewModel.output.refreshImageList = { [weak self] userInfo in
            let id = self?.detailViewModel.getThumbnailID(index: 0) ?? ""
            XCTAssert(id == "789")
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10)
    }

    func testGetThumbnailUrl(){
        let expectation = self.expectation(description: "Async call")
        self.detailViewModel.input.viewWillAppear!()
        detailViewModel.output.refreshImageList = { [weak self] userInfo in
            let thumbnailUrl = self?.detailViewModel.getThumbnailUrl(index: 0) ?? ""
            XCTAssert(thumbnailUrl == "mock thumbnailUrl")
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10)
    }
    
    func testGetThumbnailTitle(){
        let expectation = self.expectation(description: "Async call")
        self.detailViewModel.input.viewWillAppear!()
        detailViewModel.output.refreshImageList = { [weak self] userInfo in
            let thumbnailTitle = self?.detailViewModel.getThumbnailTitle(index: 0) ?? ""
            XCTAssert(thumbnailTitle == "mock title")
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10)
    }
    
    func testGetThumbnailCount(){
        let expectation = self.expectation(description: "Async call")
        self.detailViewModel.input.viewWillAppear!()
        detailViewModel.output.refreshImageList = { [weak self] userInfo in
            let thumbnailCount = self?.detailViewModel.getThumbnailCount()
            XCTAssert(thumbnailCount == 4999)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10)
    }
}
