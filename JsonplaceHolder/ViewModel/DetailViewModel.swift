//
//  DetailViewModel.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/28.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import Foundation

/**
簡略描述此方法
- Parameters:
  - num1: 參數1
  - num2: 參數2
- Returns: 回傳值
*/

class DetailViewModel {
    private var apiManager: APIManager!
    
    var input = Input()
    var output = Output()
    private var jsonResponse: JSONPlaceHolderResponseModel = []
    init(with apiManager: APIManager) {
        self.apiManager = apiManager
        transform()
    }
    
    private func transform() {
        self.input.viewWillAppear = { [weak self] in
            self?.requestImageList()
        }
    }
    
    private func requestImageList() {
        self.output.isLoading?(true)
        apiManager.getRequest(with: APIConfig.imageURL) { (jsonResponse: JSONPlaceHolderResponseModel) in
            print(jsonResponse.count)
            self.jsonResponse = jsonResponse
            self.output.refreshImageList?(self.jsonResponse)
            self.output.isLoading?(false)
        }
    }
}

extension DetailViewModel {
    struct Input {
        var viewWillAppear: (() -> Void)?
    }

    struct Output {
        var isLoading: ((Bool) -> Void)?
        var refreshImageList: ((JSONPlaceHolderResponseModel) -> Void)?
    }
}

extension DetailViewModel: ThumbnailCellConfigProtocol {
    func getThumbnailCount() -> Int{
        return self.jsonResponse.count
    }
    
    func getThumbnailUrl(index: Int) -> String {
        return "\(self.jsonResponse[index].thumbnailURL)"
    }
    
    func getThumbnailID(index: Int) -> String {
        return "\(self.jsonResponse[index].id)"
    }
    
    func getThumbnailTitle(index: Int) -> String {
        return self.jsonResponse[index].title
    }
    
    
}
