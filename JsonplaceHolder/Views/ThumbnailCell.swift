//
//  ThumbnailCell.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/28.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import UIKit

protocol ThumbnailCellConfigProtocol {
    func getThumbnailID(index: Int) -> String
    func getThumbnailTitle(index: Int) -> String
    func getThumbnailUrl(index: Int) -> String
    func getThumbnailCount() -> Int
}

class ThumbnailCell: UICollectionViewCell {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    let saImageCacheManager = SAImageCacheManager()
    var index: Int?
    deinit {
        self.saImageCacheManager.cancelSession()
    }
    func configCell(with thumbnailCellConfigProtocol: ThumbnailCellConfigProtocol) {
        
        saImageCacheManager.cancelSession()
        self.thumbnailImageView.image = UIImage()
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
        guard let index = self.index else {
            assertionFailure("ThumbnailCell should set index")
            return
        }
        self.idLabel.text = thumbnailCellConfigProtocol.getThumbnailID(index: index)
        self.titleLabel.text = thumbnailCellConfigProtocol.getThumbnailTitle(index: index)
        
        saImageCacheManager.fetchCachedDataByEtag(withUrl: thumbnailCellConfigProtocol.getThumbnailUrl(index: index),
                                           cacheKey: nil,
                                           parameters: nil,
                                           isReturnCachedImageAsDefault: true)
        { [weak self] (data, url, response, error) in
            guard let `self` = self else { return }
            guard let `data` = data else { return }
        
            //print("cell index: \(self.index) vs return index: \(index)")
            guard url == thumbnailCellConfigProtocol.getThumbnailUrl(index: self.index!) else {
                //print("this thumbnail is not for this cell")
                return
            }
            
            
            if let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.thumbnailImageView.image = image
                    self.activityIndicatorView.stopAnimating()
                    self.activityIndicatorView.isHidden = true
                }
            }
        
        }
    }
}
