//
//  ViewController.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/28.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import UIKit



class ViewController: UIViewController {

    /**
        Does something.
        - Postcondition:
          Never returns `false`.

            ```
            let result = doSomething()
            assert(result)
            ```
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func requestApiButtonTap(_ sender: UIButton) {
        let detailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    


}

