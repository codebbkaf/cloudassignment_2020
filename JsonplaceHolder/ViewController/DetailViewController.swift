//
//  DetailViewController.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/28.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var thumbnailCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    private var viewModel: DetailViewModel!
    var viewWillAppearAction: (() -> Void)?
    var cellTapAction: ((Int) -> Void)?
    private var collectionCellSpacing: CGFloat = 0.0
    private var collectionRowCount = 4

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewWillAppearAction?()
    }
    
    // MARK: - Binding
    private func bindViewModel() {
        viewModel = DetailViewModel(with: APIManager.shared)
        self.viewWillAppearAction = { [weak self] in
            self?.viewModel.input.viewWillAppear!()
        }
        
        self.cellTapAction? = { [weak self] index in
            self?.printCurrentTapIndex(index: index)
        }
        
        viewModel.output.isLoading = { [weak self] isloading in
            if isloading {
                self?.activityIndicator.isHidden = false
                self?.activityIndicator.startAnimating()
            } else {
                self?.activityIndicator.isHidden = true
                self?.activityIndicator.stopAnimating()
            }
        }
        
        viewModel.output.refreshImageList = { [weak self] userInfo in
            self?.thumbnailCollectionView.reloadData()
        }
    }
    
    // MARK: - private function
    private func printCurrentTapIndex(index: Int){
        print("tap: \(index)")
    }
    
    // MARK: - setupUI
    private func setupUI(){
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = self.collectionCellSpacing
        layout.minimumInteritemSpacing = self.collectionCellSpacing
        thumbnailCollectionView.collectionViewLayout = layout
        
    }
    
}

// MARK: - UICollectionViewDelegate
extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.getThumbnailCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbnailCell", for: indexPath) as? ThumbnailCell else { return UICollectionViewCell()}
        let thumbnailCellConfigProtocol: ThumbnailCellConfigProtocol = self.viewModel
        cell.index = indexPath.row
        cell.configCell(with: thumbnailCellConfigProtocol)
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension DetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((thumbnailCollectionView.bounds.width - (CGFloat(collectionRowCount) * 2 * self.collectionCellSpacing)) / CGFloat(collectionRowCount))
        return CGSize(width: width, height: width )
    }
}
