//
//  APIResponseModel.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/28.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import Foundation

// MARK: - JSONPlaceHolderResponseModelElement
struct JSONPlaceHolderResponseModelElement: Codable {
    let albumID, id: Int
    let title: String
    let url, thumbnailURL: String
    let aaa: String
    enum CodingKeys: String, CodingKey {
        case albumID = "albumId"
        case id, title, url
        case thumbnailURL = "thumbnailUrl"
        case aaa = "aaaa"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.albumID = try container.decodeIfPresent(Int.self, forKey: .albumID) ?? 0
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        self.url = try container.decodeIfPresent(String.self, forKey: .url) ?? ""
        self.thumbnailURL = try container.decodeIfPresent(String.self, forKey: .thumbnailURL) ?? ""
        self.aaa = try container.decodeIfPresent(String.self, forKey: .aaa) ?? ""
    }
}

typealias JSONPlaceHolderResponseModel = [JSONPlaceHolderResponseModelElement]
