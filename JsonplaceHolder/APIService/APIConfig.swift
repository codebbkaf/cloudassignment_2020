//
//  APIConfig.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/28.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import Foundation

final class APIConfig {
    static let imageURL = "https://jsonplaceholder.typicode.com/photos"
}
