//
//  APIManager.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/28.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import Foundation
import Alamofire

final class APIManager {
    
    init(isMock: Bool = false) {
        if isMock {
            MockDataManager.shared.setUpAllMockData()
        } else {
            MockDataManager.shared.removeAllMockData()
        }
    }
    
    static let shared = APIManager()
    
    func getRequest<T: Codable>(with url: String,  completion: @escaping (T) -> () ) {
        let request = AF.request(url)
        request.responseJSON { (data) in
            guard data.data != nil else { return }
//            print(data)
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(T.self, from: data.data!)
                completion(response)
            } catch {
                
            }
        }
    }
    
}
