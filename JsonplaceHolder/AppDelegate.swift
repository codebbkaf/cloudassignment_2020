//
//  AppDelegate.swift
//  JsonplaceHolder
//
//  Created by CI-Red.Tseng on 2020/8/28.
//  Copyright © 2020 CI-Red.Tseng. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }



}

